const fs = require('fs');
const path = require('path');
const exec = require('util').promisify(require('child_process').exec);
const { XMLParser } = require('fast-xml-parser');

async function parse(fn) {
  try {
    const json = await exec(`tshark -r ${fn} -T json`, { maxBuffer: 1024*1024*500 }).then(res => JSON.parse(res.stdout));
    const parser = new XMLParser({
      ignoreAttributes: false,
      attributeNamePrefix : '',
    });
    const xml = await exec(`tshark -r ${fn} -T pdml`, { maxBuffer: 1024*1024*500 }).then(res => parser.parse(res.stdout));
    const { pdml: { packet } } = xml;
    json.forEach((e, i) => {
      const { _source: { layers } } = e;
      const { proto } = packet[i];
      Object.keys(layers).forEach((name, i) => {
        const v = proto.find(e => e.name == name);
        if (v) {
          const text = {title: v.showname, content: v.field.showname ? [v.field.showname] : v.field.map(e => e.showname).filter(e => e && e.length)};
          layers[name].text = text;
        }
      });
    });
    return json
  } catch(e) {
    console.log('error', e);
    return [];
  }
}

async function arp(fn) {
  const cnt = fs.readFileSync(fn);
  return cnt.toString();
}

module.exports = {
  parse, arp,
};
