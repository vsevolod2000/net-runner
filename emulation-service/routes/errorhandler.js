var log4js = require('log4js');
var deblog = log4js.getLogger('errorhandler');

module.exports = function(err, req, res, next) {
  let error = {code: 0, message: 'Unknown server error.'};
  res.status(err.status || 400);
  // set locals, only providing error in development
  if(!err.notlog){
    try {
      deblog.debug("Error Object: "+JSON.stringify(err));
      deblog.debug("Error Message: "+JSON.stringify(err.message));
    } catch (e) {
      deblog.debug("Error json: ", err)
    }
  }

  if(err.message==undefined || err==undefined || JSON.stringify(err.message) === JSON.stringify({})){
    deblog.error("UNDEFINE error mess: "+ err);
    res.status(err.status || 400);
    return res.json({error: error});
  }
  if(~err.message.indexOf({})){
    deblog.error("UNDEFINE error mess: "+ err);
    res.status(err.status || 400);
    return res.json({error: error});
  }
  if(~err.message.indexOf('Path `password` is required')){
    error.message = 'Password required';
    error.code = 10;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Email required')){
    error.message = 'Email required';
    error.code = 11;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Login required')){
    error.message = 'Login required';
    error.code = 12;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid login')){
    error.message = 'Invalid login';
    error.code = 13;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid mail')){
    error.message = 'Invalid email';
    error.code = 14;
    return res.json({error: error});
  }
  if(~err.message.indexOf('login_1 dup key')){
    error.message = 'Dublicate login';
    error.code = 15;
    return res.json({error: error});
  }
  if(~err.message.indexOf('email_1 dup key')){
    error.message = 'Dublicate email';
    error.code = 16;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Cannot find user')){
    error.message = 'User is not found';
    error.code = 17;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Need authorization')){
    error.message = 'Need authorization';
    error.code = 18;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Must accept in mail')){
    error.message = 'Email Verification Required';
    error.code = 19;
    return res.json({error: error});
  }
  if(~err.message.indexOf('No session')){
    error.status = 250; //for not logging in web
    res.status(250);
    error.message = 'Need authorization.';
    error.code = 20;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Error fields')){
    error.message = 'Need other fields';
    error.code = 21;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Incorrect password')){
    error.message = 'Incorrect password';
    error.code = 22;
    return res.json({error: error});
  }
  if(~err.message.indexOf('BD error')){
    error.message = 'Synchronization error';
    error.code = 23;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Too many fields')){
    error.message = 'Too many fields in body';
    error.code = 50;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Too many files')){
    error.message = 'Too many files in body';
    error.code = 51;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Incr ff')){
    error.message = 'Incorrect format. Need .csv';
    error.code = 52;
    return res.json({error: error});
  }
  if(~err.message.indexOf('File too large')){
    error.message = 'File too large. Must be < 10MB';
    error.code = 53;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Max sol today')){
    error.message = 'Max solutions today';
    error.code = 54;
    return res.json({error: error});
  }
  if(~err.message.indexOf('us alr reg')){
    error.message = 'User is already registered in the championship';
    error.code = 55;
    return res.json({error: error});
  }
  if(~err.message.indexOf('adrselogin_1_champname_1_commandname_1')){
    error.message = 'Dublicate invite';
    error.code = 64;
    return res.json({error: error});
  }
  if(~err.message.indexOf('champname_1_commandname_1 dup key')){
    error.message = 'Dublicate command';
    error.code = 56;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Champ not found')){
    error.message = 'Championship not found';
    error.code = 57;
    return res.json({error: error});
  }
  if(~err.message.indexOf('File not upload')){
    error.message = 'The file was not uploaded';
    error.code = 58;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Not a member')){
    error.message = 'User is not a member of the command';
    error.code = 59;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Not find command')){
    error.message = 'Command not found';
    error.code = 60;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Not find task')){
    error.message = 'No task found in championship';
    error.code = 61;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Champ not active')){
    error.message = 'Championship is inactive';
    error.code = 62;
    return res.json({error: error});
  }
  if(~err.message.indexOf('File not found')){
    error.message = 'File not found';
    error.code = 63;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Already answered')){
    error.message = 'Invite was answered';
    error.code = 65;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invite not found')){
    error.message = 'Invite not found';
    error.code = 66;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Already in command')){
    error.message = 'User already in command';
    error.code = 67;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Max members')){
    error.message = 'Max members in command';
    error.code = 68;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Lead exeption')){
    error.message = 'Leader can\'t exclude himself';
    error.code = 69;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid commandname')){
    error.message = 'Invalid commandname';
    error.code = 70;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Max members')){
    error.message = 'Max members';
    error.code = 71;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Not found Championship')){
    error.message = 'Not found Championship';
    error.code = 72;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Max Checked')){
    error.message = 'Max checked solutions in Task';
    error.code = 73;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Not a lead')){
    error.message = 'Need be a lead';
    error.code = 74;
    return res.json({error: error});
  }
  if(~err.message.indexOf('File require')){
    error.message = 'File required!';
    error.code = 75;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Wrong format file')){
    error.message = 'Wrong format file. Need *.csv!';
    error.code = 76;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Timeout')){
    error.message = 'Timeout for this action';
    error.code = 77;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Need verify')){
    error.message = 'Need verify phone';
    error.code = 78;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Alriden')){
    error.message = 'Already identified';
    error.code = 79;
    return res.json({error: error});
  }
  if(~err.message.indexOf('CommandLimit')){
    error.message = 'Command Limit to Submit';
    error.code = 80;
    return res.json({error: error});
  }






  //64 уже существует
  if(~err.message.indexOf('Page not found')){
    error.message = 'Page not found :(';
    error.code = 404;
    error.status = 404;
    return res.json({error: error});
  }
  // if(~err.message.indexOf('')){
  //   error.message = '';
  //   error.code = ;
  // }
  if(~err.message.indexOf('Cannot reset psw')){
    error.message = 'Cannot reset psw';
    error.code = 23;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Error fields')){
    error.message = 'Need somthing fields';
    error.code = 24;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Wong Access Code')){
    error.message = 'Wrong Access Code, logged';
    error.code = 25;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Tried limit')){
    error.message = 'Tried limit exceeded';
    error.code = 26;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Dublicate phone')){
    error.message = 'Dublicate phone number';
    error.code = 27;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Wrong code')){
    error.message = 'Wrong vefify code';
    error.code = 28;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid avatar')){
    error.message = 'Invalid avatar';
    error.code = 30;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid firstname')){
    error.message = 'Invalid firstname';
    error.code = 31;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid lastname')){
    error.message = 'Invalid lastname';
    error.code = 32;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid university')){
    error.message = 'Invalid university';
    error.code = 33;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid city')){
    error.message = 'Invalid city';
    error.code = 34;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid workPlace')){
    error.message = 'Invalid workPlace';
    error.code = 35;
    return res.json({error: error});
  }
  if(~err.message.indexOf('60limit')){
    error.message = 'The code can be generated once every 60 seconds';
    error.code = 36;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Incorrect filter')){
    error.message = 'Incorrect filter avatar';
    error.code = 104;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Cast to date failed')){
    error.message = 'Incorrect date';
    error.code = 105;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid nickname')){
    error.message = 'Invalid nickname';
    error.code = 37;
    return res.json({error: error});
  }
  if(~err.message.indexOf('is not a valid enum value for path `status`')){
    error.message = 'Incorrect status';
    error.code = 106;
    return res.json({error: error});
  }






  if(~err.message.indexOf('index: champname_1 dup key')){
    error.message = 'Dublicate championship name';
    error.code = 101;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Invalid Date')){
    error.message = 'Wrong date to create a championship';
    error.code = 102;
    return res.json({error: error});
  }
  if(~err.message.indexOf('unavailable API 1')){
    error.message = 'Unavailable API. Use addsolutionDevelop';
    error.code = 103;
    return res.json({error: error});
  }
  if(~err.message.indexOf('unavailable API 2')){
    error.message = 'Unavailable API. Use addsolution';
    error.code = 104;
    return res.json({error: error});
  }
  if(~err.message.indexOf('repository unavailable')){
    error.message = 'Unavailable repository. Need like "git://github.."';
    error.code = 105;
    return res.json({error: error});
  }
  if(~err.message.indexOf('Timeout for submit')){
    error.message = 'Timeout for submit';
    error.code = 106;
    return res.json({error: error});
  }


  if(~err.message.indexOf('Unavailable')){
    error.message = 'Unavailable =(';
    error.code = 103;
    return res.json({error: error});
  }

  // render the error page
  res.status(err.status || 400);
  //res.render('error');
  res.json({error: error});
}
