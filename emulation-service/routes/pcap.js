const express           = require('express');
const log4js            = require('log4js');
const pcapController    = require('../controllers/Pcap'); 
const socket            = require('../socket');
const path              = require('path');

const logger = log4js.getLogger('pcap');
logger.level = 'debug';

const router = express.Router();

router.get('/parsed', async function(req, res, next) {
  try {
    const { root, device } = req.query;
    logger.debug(`trying to parse ${device}.pcap from ${root}`);
    if (!root || !device) {
      throw Error('incorrect args');
    }
    return res.json({
      message: 'ok',
      data: await pcapController.parse(path.resolve(__dirname, '..', 'pcaps', root, `${device}.pcap`)),
    })
  } catch(e) {
    logger.debug(e);
    return next(e);
  }
});

router.get('/arp', async function(req, res, next) {
  try {
    const { root, device } = req.query;
    logger.debug(`trying to parse ${device}.pcap from ${root}`);
    if (!root || !device) {
      throw Error('incorrect args');
    }
    return res.json({
      message: 'ok',
      data: await pcapController.arp(path.resolve(__dirname, '..', 'pcaps', root, `${device}.txt`)),
    })
  } catch(e) {
    logger.debug(e);
    return next(e);
  }
});

module.exports = router;
