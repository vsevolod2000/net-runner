const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const cors = require('cors');
const keys = require('./config/keys');
const errorhandler = require('./routes/errorhandler');
const flash = require('connect-flash');



const configRouter = require('./routes/config');
const pcapRouter = require('./routes/pcap');

const app = express();

app.use(logger('dev'));
app.use(cors(keys.corsOptions)); //need set configures
app.use(express.json({limit: '1mb'}));
app.use(express.urlencoded({limit: '1mb', extended: false }));

app.use('/config', configRouter);
app.use('/pcap', pcapRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next({message: "Page not found"});
});

// error handler
app.use(errorhandler);

module.exports = app;
