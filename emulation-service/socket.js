const log4js = require('log4js');
const logger = log4js.getLogger('io')

module.exports = {
  connection: async (socket) => {
    //logger.debug('user connected', socket.handshake.query);
    console.log('user connected', socket.handshake.query);
    const query = socket.handshake.query;
    socket.emit('hi', {message: 'hi'});
    socket.join(`thread${query.id}`);
  },
  logger: async (io, id, data) => {
    console.log('TRACE', id);
    io.to(`thread${id}`).emit('trace', data);
  }, 
  finish: async (io, id) => {
    io.to(`thread${id}`).emit('end', {});
  }, 
}
