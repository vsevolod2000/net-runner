#!/bin/bash

exists() {
  if ! [ -x "$(command -v $1)"  ]; then
    return 1
  fi
  return 0
}

npmInstall() {
  exists $1
  if [ $? -ne "0" ]; then
    npm i $1 -g
  fi
}

tools() {
  npm_tools=("pm2" "node-gyp")
echo <<EOF
  required tools: npm, ${npm_tools[*]}
EOF
}

engine() {
  backend/engine/build.sh
}
