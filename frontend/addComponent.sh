#!/bin/bash

root=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  );

name=$1
#name='da'

if [ -z "$name" ]; then
  echo [!] specify component name 1>&2;
  exit 1;
fi

read -r -d '' index << EOM
export { default } from './${name^}';
EOM

read -r -d '' globalIndex << EOM
export { default as ${name^} } from './${name,}';
EOM

read -r -d '' component << EOM
import React, { useRef, useState, useCallback, useEffect } from 'react';
import { useTranslation } from 'next-i18next';

import styles from '@/assets/components/${name^}.module.scss';
import classnames from 'classnames';

import Image from 'next/image';
import images from '@/lib/images';

export default function MyComponent({ children, className = '', namespace = 'common', ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  return (
    <div className={classnames(styles.${name,}, className)} {...props}>'...something'</div>
  );
};
EOM

read -r -d '' styles << EOM
@import 'colors.scss';
@import 'fonts.scss';
@import 'buttons.scss';
@import 'animations.scss';

.${name,} {

}
EOM

mkdir -p $root/components
if [ -d $root/components/$name ]; then
  echo [!] seems like component exists 1>&2;
  exit 1;
fi

stylesName="${name^}.module.scss"

mkdir -p $root/styles/components

if [ -d $root/styles/components/$stylesName ]; then
  echo [!] seems like component\' stylesheet exists 1>&2;
  exit 1;
fi

echo -e "$styles" > $root/styles/components/$stylesName
echo [*] added stylesheet
mkdir $root/components/${name,}
echo -e "$component" > $root/components/${name,}/${name^}.js
echo [*] added component sample code
echo -e "$index" > $root/components/${name,}/index.js
echo [*] created index file
echo -e "$globalIndex" >> $root/components/index.js
echo [*] added component to global index

exit 0
