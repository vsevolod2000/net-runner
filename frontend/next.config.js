const path = require('path');
const { i18n } = require('./next-i18next.config');

module.exports = {
  i18n, 
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  /*rewrites: async () => {
    return [
      {
        source: '/socket.io/:path*',
        destination: `http://localhost:${process.env.NEXT_PUBLIC_SOCKETIO_PORT}/socket.io`,
      }
    ]
  },*/
  /*images: {
    disableStaticImages: true,
  },*/
  webpack: (config, options) => {
    const { buildId, dev, isServer, defaultLoaders, webpack } = options;
    //console.log('CONFIG', config.module.rules);
    config.module.rules.find(e => e.loader == 'next-image-loader').test = /(?<!-url)\.(png|jpg|jpeg|gif|webp|avif|ico|bmp|svg)$/i;
    config.module.rules.push({
      test: /-url\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
      use: [
        options.defaultLoaders.babel,
        {
          loader: 'url-loader',
          options: {
            limit: 100000000,
            name: '[name].[ext]'
          }
        }
      ],
    });
    return config;
  },
};
