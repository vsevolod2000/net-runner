import React, { useState, useCallback, createContext } from 'react';
import '@/assets/globals.scss';
import Head from 'next/head';
import { MainProvider } from '@/lib/context';
import { appWithTranslation } from 'next-i18next';
import Script from 'next/script';

if (!Array.prototype.eachSlice) {
  Array.prototype.eachSlice = function (size) {
    this.arr = []
    for (let i = 0, l = this.length; i < l; i += size){
      this.arr.push(this.slice(i, i + size))
    }
    return this.arr
  };
}

if (!String.prototype.capitalize) {
  Object.defineProperty(String.prototype, 'capitalize', {
    value: function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
    },
    enumerable: false
  });
}

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>{process.env.NEXT_PUBLIC_PROJECT_NAME}</title>
        <meta id='mobile' name='viewport' content='width=900px'/>
        {/*<meta name='viewport' content='width=900, height=800,initial-scale=1, maximum-scale=1'/>*/}
      </Head>
      <MainProvider theme='dark'>
        <Component {...pageProps}/>
      </MainProvider>
    </>
  );
}

export default appWithTranslation(MyApp)
