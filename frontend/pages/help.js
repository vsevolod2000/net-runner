import React, { useRef, useState, useCallback, useEffect } from 'react';

import Image from 'next/image';
import Link from 'next/link';

import styles from '@/assets/Help.module.scss';

import { useTranslation } from 'next-i18next';
import { serverSideTranslations  } from 'next-i18next/serverSideTranslations';

import { SizeMe } from 'react-sizeme';
import classnames from 'classnames';

import { GraphProvider } from '@/lib/context';

import {
  Header,
  NetConstructor,
  SelectedCard,
  Pcap,
  PcapCard, CompCard,
} from '@/components';

import {
  PlusCircle,
  Plus,
  PlusSquare,
  Play,
  PlayFill,
  Repeat,
} from '@geist-ui/icons';

import {
  Page,
  Grid,
  Text,
  useTheme,
  Code,
  Card,
  Note,
  Dot,
  Button,
} from '@geist-ui/core';

import images from '@/lib/images';
import axios from 'axios';
import useSWR from 'swr'

export const getStaticProps = async (props) => {
  const { locale } = props;
  return {
    props: {
      ...await serverSideTranslations(locale, ['pcap', 'common']), 
    }
  }
};

async function fetcher(...args) {//TODO refactor if works
  const res = await fetch(...args)
  return res.json()
}

const GridContainer = (props) => <Grid.Container {...props}/>;

function nop(e) {
  return e;
}

const mockOptions = {
  IPs: [ '192.168.1.1', '192.168.1.3' ],
  style: { position: 'relative', transform: 'translate(0, 0)' },
  selected: {
    type: 'pc',
  },
  applications: [
    {
      type: 'ping',
      dst: '192.168.1.2',
    },
    {
      type: 'sink',
      dst: '192.168.1.2',
    },
  ],
  removeApplication: nop,
  apps: [],
  setApps: nop,
  updateApp: nop,
  submitApplication: nop,
  addApplication: nop,
  graph: { edges: [], nodes: [] },
  cur: { id: 0 },
  openPcap: nop,
  resp: { data: { pcaps: ['0-1-switch-csma.pcap', '0-2-p2p-0.pcap'] } },
  id: 0,
};

export default function Landing(props) {
  const theme = useTheme();
  const { t, i18n } = useTranslation('common', { keyPrefix: 'translation' });
  const [apps, setApps] = useState([]);
  return (
    <Page className={classnames(styles.main, styles.appear)} style={{/*minWidth: '900px', overflowX: 'auto'*/}}>
      <Header/>
      <GridContainer direction='column' className={styles.content}>
        <Text h1 align='left'>{t('help-title')}</Text>
        {Array.from({ length: 2 }, (_, i) => (
          <Text h3 key={i}>
            <GridContainer gap={2} alignItems='center'>
              {/*<Dot block type={'success'}/>*/}
              <Grid alignItems='center'>
                <Code>{t(`help-blocks.${i}.0`)}</Code>
              </Grid>
              <Grid>
                <GridContainer wrap='nowrap' gap={2}>
                  <Grid>
                    <Text span>{'— '}</Text>
                  </Grid>
                  <Grid alignItems='center'>
                    <Text span>{t(`help-blocks.${i}.1`)}</Text>
                  </Grid>
                </GridContainer>
              </Grid>
            </GridContainer>
          </Text>
        ))}
        <Text h1 align='left'>{t('help-context')}</Text>
        <GridContainer gap={2} alignItems='center' style={{marginBottom: '20px'}} wrap='nowrap'>
          <Grid style={{flexShrink: 0}}>
            <CompCard {...mockOptions} apps={apps} setApps={setApps} /*addApplication={type => setApps(state => state.concat({ type, dst: '' }))}*//>
          </Grid>
          <Grid>
            <GridContainer alignItems='center' gap={2} wrap='nowrap'>
              <Grid>
                <Text h3>{'— '}</Text>
              </Grid>
              <Grid>
                <Text h3 style={{display: 'inline-block', maxWidth: '700px'}}>
                  {t('card-help.comp')}
                </Text>
              </Grid>
            </GridContainer>
          </Grid>
        </GridContainer>
        <GridContainer gap={2} alignItems='center' style={{marginBottom: '40px'}} wrap='nowrap'>
          <Grid style={{flexShrink: 0}}>
            <PcapCard {...mockOptions} apps={apps} setApps={setApps} /*addApplication={type => setApps(state => state.concat({ type, dst: '' }))}*//>
          </Grid>
          <Grid>
            <GridContainer alignItems='center' gap={2} wrap='nowrap'>
              <Grid>
                <Text h3>{'— '}</Text>
              </Grid>
              <Grid>
                <Text h3 style={{display: 'inline-block', maxWidth: '900px'}}>
                  <Text span>{t('card-help.pcap.0')}</Text>
                  <Code>{t('card-help.pcap.1')}</Code>
                  <Text span>{'.'}</Text>
                </Text>
              </Grid>
            </GridContainer>
          </Grid>
        </GridContainer>
        <Text h1 align='left'>{t('buttons')}</Text>
        {Array.from({ length: 3 }, (_, i) => (
          <GridContainer gap={2} alignItems='center' style={{marginBottom: '20px'}} key={i}>
            <Grid>
              <Button
                iconRight={[
                  <Play color={theme.type == 'dark' ? '#fff' : '#000'} key={0}/>, 
                  <Repeat color={theme.type == 'dark' ? '#fff' : '#000'} key={1}/>,
                  <Plus color={theme.type == 'dark' ? '#fff' : '#000'} key={2}/>,
                ][i]}
                auto scale={3}
                className={styles.startBtn}/>
            </Grid>
            <Grid>
              <GridContainer alignItems='center' gap={2} wrap='nowrap'>
                <Grid>
                  <Text h3>{'— '}</Text>
                </Grid>
                <Grid>
                  <Text h3 style={{display: 'inline-block', maxWidth: '900px'}}>
                    <Text span>{t(`buttons-help.${i}`)}</Text>
                  </Text>
                </Grid>
              </GridContainer>
            </Grid>
          </GridContainer>
        ))}
      </GridContainer>
    </Page>
  );
};
