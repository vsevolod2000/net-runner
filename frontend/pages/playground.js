import React, { useRef, useState, useCallback, useEffect } from 'react';

import Image from 'next/image';
import Link from 'next/link';

import styles from '@/assets/Playground.module.scss';

import { useTranslation } from 'next-i18next';
import { serverSideTranslations  } from 'next-i18next/serverSideTranslations';

import { SizeMe } from 'react-sizeme';
import classnames from 'classnames';

import { GraphProvider } from '@/lib/context';

import {
  Header,
  NetConstructor,
  SelectedCard,
  Pcap,
  Arp,
} from '@/components';

import {
  Page,
  Grid,
  Text,
  useTheme,
} from '@geist-ui/core';

import images from '@/lib/images';
import axios from 'axios';
import useSWR from 'swr'

export const getStaticProps = async (props) => {
  const { locale } = props;
  return {
    props: {
      ...await serverSideTranslations(locale, ['pcap', 'common']), 
    }
  }
};

async function fetcher(...args) {//TODO refactor if works
  const res = await fetch(...args)
  return res.json()
}

const GridContainer = (props) => <Grid.Container {...props}/>;

export default function Landing(props) {
  const theme = useTheme();
  const { t, i18n } = useTranslation('common', { keyPrefix: 'translation' });
  return (
    <Page className={classnames(styles.main, styles.appear)}>
      <GridContainer direction='column' className={styles.content}>
        <Header/>
        <GridContainer className={styles.graphView}>
          <Grid xs={24} justify='center' /*className={styles.appear}*/>
            <GraphProvider>
              <NetConstructor/>
              <SelectedCard /*selected={selected} graphRef={graphRef}*//>
              <Pcap /*data={sampleDump}*//>
              <Arp/>
            </GraphProvider>
          </Grid>
        </GridContainer>
      </GridContainer>
    </Page>
  );
};
