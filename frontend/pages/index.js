import React, { useRef, useState, useCallback, useEffect } from 'react';

import Image from 'next/image';
//import Link from 'next/link';

import styles from '@/assets/Home.module.scss';

import { useTranslation } from 'next-i18next';
import { serverSideTranslations  } from 'next-i18next/serverSideTranslations';

import { SizeMe } from 'react-sizeme';
import classnames from 'classnames';

import { GraphProvider } from '@/lib/context';

import {
  Header,
  NetConstructor,
  SelectedCard,
  Pcap,
} from '@/components';

import {
  Page,
  Grid,
  useTheme,
  Display,
  Link,
  Text,
  Card,
  //Image,
} from '@geist-ui/core';

import images from '@/lib/images';
import axios from 'axios';
import useSWR from 'swr'

export const getStaticProps = async (props) => {
  const { locale } = props;
  return {
    props: {
      ...await serverSideTranslations(locale, ['pcap', 'common']), 
    }
  }
};

async function fetcher(...args) {//TODO refactor if works
  const res = await fetch(...args)
  return res.json()
}

const GridContainer = (props) => <Grid.Container {...props}/>;

export default function Landing(props) {
  const theme = useTheme();
  const { type, palette } = theme;
  //console.log('colors', palette);
  const [state, setState] = useState({ diff: 12 });
  useEffect(() => {
    const diff = window.innerWidth - document.body.offsetWidth;
    setState(state => ({ ...state, diff }));
  }, []);
  const { t, i18n } = useTranslation('common', { keyPrefix: 'translation' });
  return (
    <Page className={classnames(styles.main, styles.appear)} style={{/*overflow: 'hidden'*/}}>
      <GridContainer className={styles.graphView} style={{minHeight: '100vh'}}>
        <Header /*style={{ width: `calc(100% + ${state.diff}px)`, minHeight: '82px', height: '82px', maxHeight: '82px' }}*//>
        <Grid xs={24} justify='center' /*className={styles.appear}*/>
          <Display shadow 
            caption={
              <Text h3>
                {t('main-caption.0') + ' '}
                <Link href='https://www.nsnam.org/' color icon target='_blank' rel='noopener noreferrer'>{t('main-caption.1')}</Link>
                {' ' + t('main-caption.2')}
              </Text>
            }>
            {React.cloneElement(images.main, {fontColor: palette.foreground, style: { padding: '40px' }})}
          </Display>
        </Grid>
      </GridContainer>
      <GridContainer direction='column' style={{marginBottom: '120px'}}>
        <Text h1 align='center'>{t('verbose')}</Text>
        <Text h3 align='center'>
          <Text style={{maxWidth: '900px', margin: '24px auto'}}>
            {t('verbose-verbose')}
          </Text>
        </Text>
      </GridContainer>
      <Text h1 align='center'>{t('how-to-use')}</Text>
      <GridContainer gap={2} justify='center' wrap='wrap' style={{marginBottom: '80px'}}>
        {Array.from({ length: 3 }, (_, i) => (
          <Grid xs={8} justify='center' style={{minWidth: '540px'}}>
            <Card style={{width: '500px', display: 'flex', flexDirection: 'column'}} hoverable>
              <Card.Content style={{flex: 1}}>
                <div style={{position: 'relative', height: '300px', width: '100%'}}>
                  <Image src={images.steps[i]} layout='fill' objectFit='cover'/>
                </div>
                <Text h4 mb={0}>{t(`steps.${i}.0`)}</Text>
                <Text type='secondary' small>{t(`steps.${i}.1`)}</Text>
              </Card.Content>
              <Card.Footer>
                <Link block color href='/help'>{t('see-help')}</Link>
              </Card.Footer>
            </Card>
          </Grid>
        ))}
      </GridContainer>
      <GridContainer alignItems='baseline' style={{/*marginBottom: '200px'*/}}>
        <Grid>
          <Text h1 align='left'>
            <Text span>
              {t('try-it.0') + ' '}
            </Text>
            <Link color href='/playground'>{t('try-it.1') + '.'}</Link>
          </Text>
        </Grid>
        <Grid style={{transform: 'translateY(6px)'}}>
          <Image src={images.lemur} width={100} height={100}/>
        </Grid>
      </GridContainer>
      <Page.Footer>
        <Text h4>
          {t('footer')}
        </Text>
      </Page.Footer>
    </Page>
  );
};
