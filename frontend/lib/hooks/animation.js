import React, { useRef, useState, useCallback, useEffect, createContext } from 'react';

const seqRex = /Seq=\d+/;
const ackRex = /Ack=\d+/;
const synAck = /\[(SYN|ACK|\|)+\]/;
const icmpRex = /ns3::Icmpv4Header (\(type=\d+, code=\d+\))/;
function getPacketTitle(pkt) {
  /*{
    "fId": "0",
    "fbTx": "0.003",
    "lbTx": "0.003102399",
    "meta-info": "ns3::EthernetHeader ( length/type=0x806, source=00:00:00:00:00:05, destination=ff:ff:ff:ff:ff:ff ) ns3::ArpHeader (request source mac: 00-06-00:00:00:00:00:05 source ipv4: 192.168.1.1 dest ipv4: 192.168.1.2) Payload (size=18) ns3::EthernetTrailer (fcs=0)",
    "tId": "2",
    "fbRx": "0.005102399",
    "lbRx": "0"
  }*/
  const meta = pkt['meta-info'];
  //console.log('meta', meta);
  const headerRex = /ns3::([a-zA-Z0-9]+)Header/g;
  let res = '';
  let match;
  while ((match = headerRex.exec(meta))) {
    res = match[1];
  }
  if (res == 'Arp') {
    if (meta.includes('reply')) {
      res += ' reply';
    }
    else {
      res += ' request';
    }
  }
  if (res == 'Tcp') {
    if (synAck.test(meta)) {
      res += ' ' + synAck.exec(meta)[0];
    }
    if (seqRex.test(meta)) {
      res += ' ' + seqRex.exec(meta)[0];
    }
    if (ackRex.test(meta)) {
      res += ' ' + ackRex.exec(meta)[0];
    }
  }
  if (res == 'Icmpv4') {
    if (icmpRex.test(meta)) {
      res += icmpRex.exec(meta)[1];
    }
  }
  return res;
}

function edgeExists({ graph, from, to }) {
  const { edges } = graph;
  return edges.find(e => (e.source == from.id && e.target == to.id) || (e.source == to.id && e.target == from.id));
}

function edgeExistsID({ graph, from, to }) {
  const { edges } = graph;
  return edges.find(e => (e.source == from && e.target == to) || (e.source == to && e.target == from));
}

function distance(a, b) {
  return Math.sqrt((b.x - a.x)**2 + (b.y - a.y)**2);
}

function findHub({ from, to, graph, src = '', dst = '' }) {
  //console.log('find hub: ', src, dst);
  const { edges, nodes } = graph;
  for (const hub of nodes.filter(e => e.type == 'hub')) {
    const a = edgeExists({graph, from, to: hub});
    const b = edgeExists({graph, to, from: hub});
    if (a && b) {
      //console.log('HUB CANDIDATE', hub, a, b);
      if (src.length > 0 && dst.length > 0) {
        const ips = [a.sourceIP, a.targetIP, b.sourceIP, b.targetIP];
        if (ips.includes(src)/* && ips.includes(dst)*/) {
          return hub;
        }
      }
      else {
        return hub;
      }
    }
  }
  return null;
}

function hubDfs({ graph, from, to, used }) {
  const { edges, nodes } = graph;
  const curEdges = edges.filter(e => e.source == from || e.target == from).map(e => e.source == from ? e : { source: e.target, target: e.source } );
  for (const edge of curEdges) {
    if (used[edge.target]) continue;
    if (edge.target == to) return [from, to];
    if (nodes[edge.target]?.type != 'hub') continue;
    const can = hubDfs({ from: edge.target, to, graph, used: { ...used, [from]: true} });
    if (can) return [from, ...can];
  }
  return null;
}

function calcHubPaths({ packets, graph }) {
  const { nodes, edges } = graph;
  const res = Array.from({ length: nodes.length }, () => []);
  for (const pkt of packets) {
    const from = parseInt(pkt.fId);
    const to = parseInt(pkt.tId);
    if (!edgeExistsID({ graph, from, to }) && !res[from][to]) {
      //const used = Array(nodes.length);
      const used = {};
      used[from] = true;
      const path = hubDfs({ graph, from, to, used });
      if (path) {
        res[from][to] = Array.from({ length: path.length - 1 }, (_, i) => [ nodes[path[i]], nodes[path[i + 1]] ]);
        //res[from][to] = Array.from({ length: path.length - 1 }, (_, i) => [ path[i], path[i + 1] ]);
        //console.log('hub path', from, to, res[from][to]);
        //console.log('hub path', from, to, path);
      }
    }
  }
  return res;
}

const srcIPRex = /source ipv4: ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/;
const dstIPRex = /dest ipv4: ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/;
const fetchSrcIP = (p) => (srcIPRex.exec(p['meta-info']) || ['', ''])[1];
const fetchDstIP = (p) => (dstIPRex.exec(p['meta-info']) || ['', ''])[1];

function koef(n) {
  return Math.min(Math.max(n, 0), 1);
}

export default function useAnimation({ onFinish, options, anime, graph, active }) {
  const [animationNodes, setAnimationNodes] = useState([]);
  const start = useRef(null);
  const allSent = useRef(false);
  const animeData = useRef({});
  const { speed, animeLen } = options;
  const duration = animeLen * 1000 / speed;
  const timing = e => e;
  const finish = () => {
    setAnimationNodes([]);
    start.current = null;
    animeData.current = {};
    onFinish();
  };
  useEffect(() => {
    if (active) {
      if (!start.current) {
        start.current = performance.now();
        allSent.current = false;
        const packets = (anime?.anim?.p || []).map(p => ({ ...p, title: getPacketTitle(p) }));
        animeData.current = {
          packets,
          animeGraph: graph.nodes.reduce((res, e) => ({ ...res, [e.id]: e }), {}),
          hubPaths: calcHubPaths({ graph, packets }),
        };
        console.log('hub paths', animeData.current.hubPaths);
        //console.log('graph', graph);
        //return;
      }
      const animID = requestAnimationFrame(animate);
      return () => cancelAnimationFrame(animID);
    }
    else if (start.current) {
      //alert('finish 1');
      finish();
    }
    return () => null;
  }, [active, animationNodes]);
  const animate = (time) => {
    let timeFraction = (time - start.current) / duration;
    if (timeFraction > 1) timeFraction = 1;
    let progress = timing(timeFraction*duration);
    if (timeFraction < 1 && active && !allSent.current) {
      //requestAnimationFrame(animate);
      draw(progress);
    }
    else {
      //alert('finish 2 ' + timeFraction + ' ' + active + ' ' + allSent.current);
      finish();
    }
  }
  const draw = useCallback((progress) => {
    //console.log('draw', draw);
    const { speed } = options; 
    if (!anime?.anim?.p) {
      return finish();
    }
    //const { data: { anime } } = resp;
    //const packets = anime.anim.p.map(p => ({ ...p, title: getPacketTitle(p) }));
    //const animeGraph = graph.nodes.reduce((res, e) => ({ ...res, [e.id]: e }), {});
    const { packets, animeGraph, hubPaths } = animeData.current;
    const { nodes, edges } = graph;
    setAnimationNodes(state => {
      const res = [];
      let sent = 0;
      let id = 0;
      const time = progress*speed;
      //const time = progress;
      for (const p of packets) {
        //const start = parseFloat(p.fbTx)/speed;
        //const stop = parseFloat(p.fbRx)/speed;
        const startInit = parseFloat(p.fbTx);
        const stopInit = parseFloat(p.fbRx);
        const start = startInit*200;
        const stop = stopInit*200;
        //const stop = start + (stopInit - start)*200;
        //console.log('stop', stopInit, stop);
        const from = animeGraph[p.fId];
        const to = animeGraph[p.tId];
        if (time > stop) {
          sent += 1;
        }
        if (time >= start && time <= stop) {
          console.log('packet packet packet!!!');
          const k = (time - start)/(stop - start);
          const o = {
            id: `anime${id}`,
            type: 'anime',
            title: p.title,
          };
          //console.log('catch up', time, start, stop);
          if (edgeExists({ graph, from, to })) {
            res.push({
              ...o,
              x: from.x + k*(to.x - from.x),
              y: from.y + k*(to.y - from.y),
            });
            id += 1;
          }
          else {//there is a hub between them
            const hubPath = hubPaths[from.id]?.[to.id];
            if (!hubPath) {
              console.log('error', from, to, graph, fetchSrcIP(p), fetchDstIP(p));
            }
            else {
              const ls = hubPath.map(([f, t]) => distance(f, t));
              const lks = ls.reduce((res, e) => res + e, 0);
              const sd = 1/ls.length;
              const n = Math.floor(k / sd);
              const [a, b] = hubPath[n];
              res.push({
                ...o,
                x: a.x + (k - sd*n)/sd*(b.x - a.x),
                y: a.y + (k - sd*n)/sd*(b.y - a.y),
              });
              /*res.push({
                ...o,
                x: from.x + k/sd*(hub.x - from.x),
                y: from.y + k/sd*(hub.y - from.y),
              });
              res.push({
                ...o,
                x: hub.x + koef(k - sd)/sd*(to.x - hub.x),
                y: hub.y + koef(k - sd)/sd*(to.y - hub.y),
                //x: hub.x + koef(k - Math.min(sd, lk1))/Math.min(lk2, sd)*(to.x - hub.x),
                //y: hub.y + koef(k - Math.min(sd, lk1))/Math.min(lk2, sd)*(to.y - hub.y),
              });*/
              id += 1;
            }
          }
        }
      }
      //console.log('sent', sent);
      if (sent == packets.length) {
        //alert('da');
        //setAllSent(true);
        allSent.current = true;
      }
      //console.log('all sent', allSent, sent, packets.length);
      //console.log('da', progress*speed, res);
      return res;
    });
  }, [setAnimationNodes, options, anime, graph, active, finish]);
  return animationNodes;
}
