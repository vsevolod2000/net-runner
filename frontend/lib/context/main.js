import React, { useRef, useState, useCallback, useEffect, createContext } from 'react';
import axios from 'axios';

import { GeistProvider, CssBaseline  } from '@geist-ui/core';

const MainContext = createContext();

const dev = process.env.NODE_ENV !== 'production';

export function Provider({ children, namespace, theme }) {
  //const [themeType, setThemeType] = useState('dark');
  const [themeType, setThemeType] = useState(theme);
  const switchThemes = useCallback(() => {
    setThemeType(last => (last === 'dark' ? 'light' : 'dark'))
  }, [setThemeType]);
  const value = {
    switchThemes,
    themeType,
  };
  return (
    <GeistProvider themeType={themeType}>
      <CssBaseline />
      <MainContext.Provider value={value}>
        {children}
      </MainContext.Provider>
    </GeistProvider>
  );
}

export function useContext() {
  const state = React.useContext(MainContext);
  if (state === undefined) {
    throw new Error('useContext must be used within ContextProvider');
  }
  return state;
}

export function withContext(config) {
  return function(Component) {
    return function MainPage(props) { 
      return (
        <Provider {...config}>
          <Component {...props}/>
        </Provider>
      );
    }
  }
}
