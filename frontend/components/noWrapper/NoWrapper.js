import React, { useRef, useState, useCallback, useEffect } from 'react';
import styles from '@/assets/components/NoWrapper.module.scss';
import classnames from 'classnames';

//NOTE careful with {} in strings

const rex = /\{.*?\}/g;

export default function MyComponent(props) {
  const { str } = props;
  const res = [];
  let lastIdx = 0;
  let match;
  while ((match = rex.exec(str)) !== null) {
    if (lastIdx != match.index) {
      res.push({start: lastIdx, end: match.index, type: 'wrap'});
    }
    res.push({start: match.index, end: rex.lastIndex, type: 'nowrap'});
    lastIdx = rex.lastIndex;
  }
  if (lastIdx != str.length) {
    res.push({start: lastIdx, end: str.length, type: 'wrap'});
  }
  return (
    <>
    {res.map((e, i) => (
      <span className={e.type == 'nowrap' ? styles.nowrap : ''} key={i}>{e.type == 'nowrap' ? str.slice(e.start, e.end).slice(1, -1) : str.slice(e.start, e.end)}</span>
    ))}
    </>
  )
};
