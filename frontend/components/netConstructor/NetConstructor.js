import React, { useRef, useState, useCallback, useEffect } from 'react';
import { useTranslation } from 'next-i18next';

import styles from '@/assets/components/NetConstructor.module.scss';
import classnames from 'classnames';

import Image from 'next/image';
import images from '@/lib/images';

import { withGraph, useGraph } from '@/lib/context';

import {
  PlusCircle,
  Plus,
  PlusSquare,
  Play,
  PlayFill,
  Repeat,
  Square,
  Settings,
} from '@geist-ui/icons';

import { 
  useTheme,
  Card,
  Text,
  Popover,
  Input,
  Button,
  Link,
  User,
} from '@geist-ui/core'

/*import {
  GraphView, // required
  Edge, // optional
  Node, // optional
  BwdlTransformer, // optional, Example JSON transformer
  GraphUtils // optional, useful utility functions
} from 'react-digraph';*/

import dynamic from 'next/dynamic';
const GraphView = dynamic(() => import('react-digraph').then(mod => mod.GraphView), { ssr: false  });

const GetGraphConfig = (theme) => ({
  NodeTypes: {
    empty: { // required to show empty nodes
      typeText: 'None',
      shapeId: '#empty', // relates to the type property of a node
      shape: (
        <symbol viewBox='0 0 100 100' id='empty' key='0'>
        </symbol>
      )
    },
    anime: {
      typeText: 'None',
      shapeId: '#anime', // relates to the type property of a node
      shape: (
        <symbol viewBox='0 0 20 20' width='30' height='30' id='anime' key='0'>
          <circle cx='10' cy='10' r='10' fill={theme.type == 'dark' ? theme.palette.cyan : theme.palette.violet}></circle>
        </symbol>
      )
    },
    pc: { // required to show empty nodes
      typeText: 'Node',
      shapeId: '#pc', // relates to the type property of a node
      shape: (
        <symbol viewBox='0 0 500 500' id='pc' key='0'>
          <g xmlns='http://www.w3.org/2000/svg'>
            <g>
              <path fill='#AFB6BB' d='M152.5,447.5h20h140h20c5.5,0,10,4.5,10,10v10v10h-200v-10v-10C142.5,452,147,447.5,152.5,447.5z'/>
              <rect x='172.5' y='377.5' fill='#E7ECED' width='140' height='70'/>
              <path fill='#31C0D8' d='M477.5,17.5v270H7.5v-270c0-5.5,4.5-10,10-10h450C473,7.5,477.5,12,477.5,17.5z'/>
              <path fill='#AFB6BB' d='M477.5,287.5v80c0,5.5-4.5,10-10,10h-155h-140h-155c-5.5,0-10-4.5-10-10v-80H477.5z'/>
            </g>
            <g id='XMLID_42_'>
              <g>
                <path fill='#E7ECED' d='M242.5,312.5c11,0,20,9,20,20s-9,20-20,20s-20-9-20-20S231.5,312.5,242.5,312.5z'/>
              </g>
              <g>
                <path fill='#231F20' d='M467.5,0h-450C7.851,0,0,7.851,0,17.5v350c0,9.649,7.851,17.5,17.5,17.5H165v55h-12.5     c-9.649,0-17.5,7.851-17.5,17.5V485h215v-27.5c0-9.649-7.851-17.5-17.5-17.5H320v-55h147.5c9.649,0,17.5-7.851,17.5-17.5v-350     C485,7.851,477.149,0,467.5,0z M17.5,15h450c1.355,0,2.5,1.145,2.5,2.5V280H15V17.5C15,16.145,16.145,15,17.5,15z M335,457.5V470     H150v-12.5c0-1.355,1.145-2.5,2.5-2.5h180C333.855,455,335,456.145,335,457.5z M180,440v-55h125v55H180z M467.5,370h-450     c-1.355,0-2.5-1.145-2.5-2.5V295h455v72.5C470,368.855,468.855,370,467.5,370z'/>
                <path fill='#231F20' d='M242.5,305c-15.163,0-27.5,12.336-27.5,27.5s12.337,27.5,27.5,27.5s27.5-12.336,27.5-27.5     S257.663,305,242.5,305z M242.5,345c-6.893,0-12.5-5.607-12.5-12.5s5.607-12.5,12.5-12.5s12.5,5.607,12.5,12.5     S249.393,345,242.5,345z'/>
              </g>
            </g>
          </g>
        </symbol>
      )
    },
    switch: {
      typeText: 'LAN',
      shapeId: '#switch',
      shape: (
        <symbol viewBox='0 0 500 500' id='switch' key='0'>
          <g xmlns='http://www.w3.org/2000/svg'>
            <g>
              <rect x='42.5' y='281.125' fill='#31C0D8' width='200' height='50'/>
              <path fill='#AFB6BB' d='M477.5,246.125v120h-20h-430h-20v-120h380h40H477.5z M242.5,331.125v-50h-200v50H242.5z'/>
            </g>
            <g>
              <path fill='#231F20' d='.5v32.75h15v-32.75h364v32.75h15v-32.75H465v-40h20    V238.625z M420,53.625v35h-25v-35H420z M395,103.625h25v135h-25V103.625z M450,398.625H35v-25h415V398.625z M470,358.625H15v-105    h455V358.625z'/>
              <path fill='#231F20' d='M250,273.625H35v65h215V273.625z M50,288.625h139v35H50V288.625z M235,323.625h-31v-35h31V323.625z    '/>
              <rect x='435' y='271.125' fill='#231F20' width='15' height='70'/>
              <rect x='395' y='271.125' fill='#231F20' width='15' height='70'/>
              <rect x='355' y='271.125' fill='#231F20' width='15' height='70'/>
              <rect x='315' y='271.125' fill='#231F20' width='15' height='70'/>
              <rect x='275' y='271.125' fill='#231F20' width='15' height='70'/>
            </g>
          </g>
        </symbol>
      )
    },
    hub: {
      typeText: 'HUB',
      shapeId: '#hub',
      shape: (
        <symbol id='hub' viewBox='0 0 500 400' width='200' height='100' key='0'>
          <g xmlns='http://www.w3.org/2000/svg'>
              <rect x='0' y='140' width='100' height='70' fill='transparent'></rect>
              <rect x='0' y='140' fill='#AFB6BB' width='470' height='196'/>
              <rect x='46' y='190' fill='#E7ECED' width='76' height='48'/>
              <rect x='66' y='172' fill='#E7ECED' width='36' height='20'/>
              <rect x='166' y='190' fill='#E7ECED' width='76' height='48'/>
              <rect x='186' y='172' fill='#E7ECED' width='36' height='20'/>
              <rect x='286' y='190' fill='#E7ECED' width='76' height='48'/>
              <rect x='306' y='172' fill='#E7ECED' width='36' height='20'/>
              <path fill='#231F20' d='M462.5,127H7.5c-4.143,0-7.5,3.357-7.5,7.5v201c0,4.143,3.357,7.5,7.5,7.5h455c4.143,0,7.5-3.357,7.5-7.5v-201 C470,130.357,466.643,127,462.5,127z M455,298H77.5c-4.143,0-7.5,3.357-7.5,7.5s3.357,7.5,7.5,7.5H455v15H15v-15h32.5 c4.143,0,7.5-3.357,7.5-7.5s-3.357-7.5-7.5-7.5H15V142h440V298z'/>
              <path fill='#231F20' d='M38.5,253h90c4.143,0,7.5-3.357,7.5-7.5v-60c0-4.143-3.357-7.5-7.5-7.5H116v-12.5c0-4.143-3.357-7.5-7.5-7.5h-50 c-4.143,0-7.5,3.357-7.5,7.5V178H38.5c-4.143,0-7.5,3.357-7.5,7.5v60C31,249.643,34.357,253,38.5,253z M46,193h12.5 c4.143,0,7.5-3.357,7.5-7.5V173h35v12.5c0,4.143,3.357,7.5,7.5,7.5H121v45H46V193z'/>
              <path fill='#231F20' d='M159.5,253h90c4.143,0,7.5-3.357,7.5-7.5v-60c0-4.143-3.357-7.5-7.5-7.5H237v-12.5c0-4.143-3.357-7.5-7.5-7.5h-50 c-4.143,0-7.5,3.357-7.5,7.5V178h-12.5c-4.143,0-7.5,3.357-7.5,7.5v60C152,249.643,155.357,253,159.5,253z M167,193h12.5 c4.143,0,7.5-3.357,7.5-7.5V173h35v12.5c0,4.143,3.357,7.5,7.5,7.5H242v45h-75V193z'/>
              <path fill='#231F20' d='M280.5,253h90c4.143,0,7.5-3.357,7.5-7.5v-60c0-4.143-3.357-7.5-7.5-7.5H358v-12.5c0-4.143-3.357-7.5-7.5-7.5h-50 c-4.143,0-7.5,3.357-7.5,7.5V178h-12.5c-4.143,0-7.5,3.357-7.5,7.5v60C273,249.643,276.357,253,280.5,253z M288,193h12.5 c4.143,0,7.5-3.357,7.5-7.5V173h35v12.5c0,4.143,3.357,7.5,7.5,7.5H363v45h-75V193z'/>
          </g>
        </symbol>
      )
    }
  },
  NodeSubtypes: {},
  EdgeTypes: {
    emptyEdge: {  // required to show empty edges
      shapeId: '#emptyEdge',
      shape: (
        <symbol viewBox='0 0 50 50' id='emptyEdge' key='0'>
        </symbol>
      )
    }
  }
})

const NODE_KEY = 'id'       // Allows D3 to correctly update DOM


const getFillComponent = (theme) => {
  const { palette } = theme;
  //const rectCol = palette.background;
  //const dotCol = theme.type == 'dark' ? palette.violet : palette.cyan;
  const Res = () => (
    <>
    <pattern id='mylightgrid' width='36' height='36' patternUnits='userSpaceOnUse'>
      <g>
        <rect fill={'#fff'} width='36' height='36' x='0' y='0'></rect>
        <circle cx='18' cy='18' r='2' fill={palette.cyan}></circle>
      </g>
    </pattern>
    <pattern id='mydarkgrid' width='36' height='36' patternUnits='userSpaceOnUse'>
      <g>
        <rect fill={'#000'} width='36' height='36' x='0' y='0'></rect>
        <circle cx='18' cy='18' r='2' fill={palette.violet}></circle>
      </g>
    </pattern>
    </>
  )
  Res.displayName = 'FillComponent';
  return Res;
}

function AddMenu({ namespace = 'common', ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const theme = useTheme();
  const { addNode, stage } = useGraph();
  const content = () => (
    <>
      <Popover.Item title>
        <span>{t('add-device')}</span>
      </Popover.Item>
      <Popover.Item onClick={addNode.bind(null, 'hub')}>
        <User src={images.graphIcons.hub.src} name={t('hub-device')} className={styles.addMenuItem}/>
      </Popover.Item>
      <Popover.Item onClick={addNode.bind(null, 'pc')}>
        <User src={images.graphIcons.comp.src} name={t('comp')} className={styles.addMenuItem}/>
      </Popover.Item>
      <Popover.Item onClick={addNode.bind(null, 'switch')}>
        <User src={images.graphIcons.lan.src} name={t('switch-device')} className={styles.addMenuItem}/>
      </Popover.Item>
      <Popover.Item line />
      {/*<Popover.Item>
        <span>Command-Line</span>
      </Popover.Item>*/}
    </>
  );
  /*<PlusCircle size={80} color={theme.type == 'dark' ? '#fff' : '#000'} className={styles.addMenuIcon}/>*/
  return (
    <Popover content={content} className={styles.addMenu} placement='top' disableItemsAutoClose>
      <Button iconRight={<Plus color={theme.type == 'dark' ? '#fff' : '#000'}/>} className={styles.addMenuIcon} auto scale={3} disabled={stage != 0}/> 
    </Popover>
  );
}

function Options({ namespace = 'common', ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const theme = useTheme();
  const { options, setOptions, stage } = useGraph();
  const updateField = useCallback((fld, evt) => {
    setOptions(options => ({ ...options, [fld]: parseFloat(evt.target.value) || options[fld], [fld + 'Local']: evt.target.value }));
  }, [setOptions]);
  const content = () => (
    <>
      <Popover.Item title>
        <span>{t('options')}</span>
      </Popover.Item>
      <Popover.Item>
        <Input label={t('option-blocks.anime-len')} placeholder='0.5' value={options.animeLenLocal} onChange={updateField.bind(null, 'animeLen')}/>
      </Popover.Item>
      <Popover.Item>
        <Input label={t('option-blocks.speed')} placeholder='0.0015' value={options.speedLocal} onChange={updateField.bind(null, 'speed')}/>
      </Popover.Item>
    </>
  );
  /*<PlusCircle size={80} color={theme.type == 'dark' ? '#fff' : '#000'} className={styles.addMenuIcon}/>*/
  return (
    <Popover content={content} className={styles.optionsMenu} placement='top' disableItemsAutoClose>
      <Button iconRight={<Settings color={theme.type == 'dark' ? '#fff' : '#000'}/>} className={styles.addMenuIcon} auto scale={3} disabled={stage != 0}/> 
    </Popover>
  );
}

function Start(props) {
  const theme = useTheme();
  const { stage, launch, reset, stopAnimation } = useGraph();
  const nop = useCallback(e => e);
  //alert(stage);
  return (
    <Button 
      onClick={stage == 0 ? 
               launch : 
               stage == 2 ? 
               stopAnimation :
               stage == 3 ? 
               reset : 
               nop
      }
      loading={stage == 1}
      disabled={stage == 1} 
      //disabled={stage == 2} 
      iconRight={
        stage == 0 ? 
          <Play color={theme.type == 'dark' ? '#fff' : '#000'}/> :
        stage == 2 ?
          <Square color={theme.type == 'dark' ? '#fff' : '#000'}/> :
          <Repeat color={theme.type == 'dark' ? '#fff' : '#000'}/> 
      } 
      auto scale={3} 
      className={styles.startBtn}/>
  )
}

const NetConstructor = /*withGraph()(*/({ children, className = '', namespace = 'common', ...props }) => {
  const theme = useTheme();
  //alert(theme.palette.violet);

  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });

  const { 
    stage,
    onSelect, graphRef,
    graph: { nodes, edges }, 
    //selected, 
    rawSelected, 
    keyPrefix,
    onCreateEdge,
    onDeleteSelected,
  } = useGraph();
  const GraphConfig = GetGraphConfig(theme);
  const NodeTypes = GraphConfig.NodeTypes;
  const NodeSubtypes = GraphConfig.NodeSubtypes;
  const EdgeTypes = GraphConfig.EdgeTypes;


  const graphProps = {
    readOnly: stage != 0 && stage != 3,
    nodeKey: NODE_KEY,
    nodes: nodes,
    edges: edges,
    onCreateEdge,
    selected: rawSelected,
    //selected: selected,
    nodeTypes: NodeTypes,
    nodeSubtypes: NodeSubtypes,
    edgeTypes: EdgeTypes,
    allowMultiselect: false, // true by default, set to false to disable multi select.
    renderDefs: getFillComponent(theme),
    showGraphControls: false,
    centerNodeOnMove: true,
    disableBackspace: false,
    onDeleteSelected,
    canDeleteSelected: (...args) => true,
    renderNodeText: (...args) => {
      const [ node ] = args;
      return (
        <text textAnchor='middle' xmlns='http://www.w3.org/2000/svg' fill={theme.type == 'dark' ? '#fff' : '#000'}>
          <tspan x='0' dy={node.type == 'switch' ? '0' : '-20'} xmlns='http://www.w3.org/2000/svg' 
            fontSize='24px' 
            fontFamily={theme.font.sans}>{
              node.type == 'anime' ? node.title : /*node.id*/ ''
            }</tspan>
        </text>
      )
    },
    edgeArrowSize: 0.00001,
    onSelect,
    //onCreateNode={onCreateNode}
    //onUpdateNode={onUpdateNode}
    //onDeleteNode={onDeleteNode}
    //onCreateEdge={onCreateEdge}
    //onSwapEdge={onSwapEdge}
    //onDeleteEdge={onDeleteEdge}
  };

  return (
    <Card className={styles.netConstructor} id='graph'>
      <GraphView 
        {...graphProps}
        backgroundFillId={theme.type == 'dark' ? '#mydarkgrid' : '#mylightgrid'}
        key={theme.type == 'dark' ? 'dark' + keyPrefix : 'light' + keyPrefix}/> 
      <Start/>
      <Options/>
      <AddMenu/>
      <div className={styles.sizer} ref={graphRef}/>
    </Card>
  );
}/*)*/;

NetConstructor.displayName= 'NetConstructor';

export default NetConstructor;
