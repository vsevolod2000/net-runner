import React, { useRef, useState, useCallback, useEffect } from 'react';
import styles from '@/assets/components/HighlightText.module.scss';
import classnames from 'classnames';

const orRegExp = (...regexes) => new RegExp(regexes.map(regex => regex.source).join('|'), 'gi');

const re = {
  numbers: /([0-9][0-9 ]*)/,
  email: /((([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,}))/,
};

export default function HighlightText({ str, target = 'numbers' }) {
  const res = [];
  let lastIdx = 0;
  let match;
  const targets = target.split('|').map(e => re[e]);
  if (targets.length == 0 || targets.find(e => !Boolean(e))) {
    return (
      <span>{str}</span>
    );
  }
  const rex = orRegExp(...targets);
  while ((match = rex.exec(str)) !== null) {
    if (lastIdx != match.index) {
      res.push({start: lastIdx, end: match.index, type: 'plain'});
    }
    res.push({start: match.index, end: rex.lastIndex, type: 'bold'});
    lastIdx = rex.lastIndex;
  }
  if (lastIdx != str.length) {
    res.push({start: lastIdx, end: str.length, type: 'plain'});
  }
  //console.log('hightlight numbers', res);
  return (
    <>
    {res.map((e, i) => (
      <span className={e.type == 'bold' ? styles.bold : ''} key={i}>{str.slice(e.start, e.end)}</span>
    ))}
    </>
  )
}
