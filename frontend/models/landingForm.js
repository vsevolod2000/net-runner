import mongoose from 'mongoose';
import { validate as emailValidator } from 'email-validator';
import uniqueValidator from 'mongoose-unique-validator';

const LandingFormSchema = new mongoose.Schema({

});

LandingFormSchema.post('save', async function(doc) {

});

export const stringifyError = (e) => e.errors.name ? [e.errors.name.message, 0] : e.errors.email ? [e.errors.email.message, 1] : ['error', 0];

let model;
if (process.env.NODE_ENV == 'development') {
  if (mongoose.models.LandingForm) {
    delete mongoose.connection.models.LandingForm;
  }
  model = mongoose.model('LandingForm', LandingFormSchema);
}
else {
  model = mongoose.models.LandingForm || mongoose.model('LandingForm', LandingFormSchema);
}

export default model;
