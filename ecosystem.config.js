const fs = require('fs');
const path = require('path');

module.exports = {
  apps: [
    {
      name: 'net-runner-frontend', 
      cwd: './frontend',
      script: `./start.sh`,
      watch: ['next.config.js', 'jsconfig.json', 'package.json', '.env'],
      watch_delay: 1000,
      ignore_watch : ['node_modules'],
    },
    {
      name: 'net-runner-emulation-service', 
      cwd: './emulation-service',
      script: `./start.sh`,
      //watch: ['routes', 'controllers', 'socket.js'],
      watch_delay: 1000,
      ignore_watch : ['node_modules'],
    },
  ],
};
